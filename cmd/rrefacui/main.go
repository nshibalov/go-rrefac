package main

import (
	"context"
	"fmt"
	"go-rrefac/internal/rkn"
	"log"

	"go.uber.org/zap"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

const (
	AppName = "rrefac"
)

func main() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatal(err)
	}

	ui := NewUI(logger)

	ui.Run()
}

type UI struct {
	logger *zap.Logger

	app    fyne.App
	window fyne.Window

	config *rkn.Config

	openFileDialog         *dialog.FileDialog
	openFileDialogLocation fyne.ListableURI
}

func (ui *UI) Layout() {
	settingsButton := widget.NewButtonWithIcon("", theme.SettingsIcon(), func() {})

	ui.window.SetContent(container.NewVBox(
		container.NewHBox(
			widget.NewButton("Выбрать файл...", func() {
				ui.openFileDialog.SetLocation(ui.openFileDialogLocation)
				ui.openFileDialog.Show()
			}),
			layout.NewSpacer(),
			settingsButton,
		),
		layout.NewSpacer(),
	))
}

func (ui *UI) openFileDialogCallback(closer fyne.URIReadCloser, err error) {
	if err != nil {
		ui.logger.Debug(fmt.Sprintf("error selecting file: %s", err))

		dialog.NewError(err, ui.window).Show()

		return
	}

	if closer == nil {
		ui.logger.Debug("open file dialog: nothing selected")
		return
	}

	defer closer.Close()

	api, err := rkn.NewAPI(ui.logger)
	if err != nil {
		ui.logger.Debug(fmt.Sprintf("error creating api: %s", err))

		dialog.NewError(err, ui.window).Show()

		return
	}

	report := rkn.NewREFacilityReport(ui.config, api)

	reportFileName, err := report.ProcessXLSX(
		context.Background(),
		closer.URI().Path(),
		"",
		rkn.DefaultSourceXLSXLSheet,
		rkn.DefaultSourceXLSXRowOffset)

	if err != nil {
		ui.logger.Debug(fmt.Sprintf("error generating report: %s", err))

		dialog.NewError(err, ui.window).Show()

		return
	}

	ui.logger.Debug(fmt.Sprintf("report file name: %s", reportFileName))

	dialog.NewInformation(
		"Готово",
		"Файл успешно сохранён",
		ui.window).Show()
}

func (ui *UI) Run() {
	ui.window.ShowAndRun()
}

func NewUI(logger *zap.Logger) *UI {
	a := app.New()

	window := a.NewWindow(fmt.Sprintf("%s - РКН сборщик данных о BTS", AppName))
	window.Resize(fyne.Size{Width: 800, Height: 600})

	config := rkn.NewConfig()

	ui := &UI{
		logger: logger,
		app:    a,
		window: window,
		config: config,
	}

	ui.openFileDialog = dialog.NewFileOpen(ui.openFileDialogCallback, window)

	ui.Layout()

	return ui
}
