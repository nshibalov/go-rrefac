package main

import (
	"context"
	"fmt"
	"go-rrefac/internal/rkn"
	"log"
	"os"

	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatal(err)
	}

	if len(os.Args) < 2 {
		logger.Fatal("xlsx file not specified")
	}

	api, err := rkn.NewAPI(logger)
	if err != nil {
		logger.Fatal(fmt.Sprintf("error creating api: %s", err))
	}

	report := rkn.NewREFacilityReport(rkn.NewConfig(), api)

	_, err = report.ProcessXLSX(
		context.Background(),
		os.Args[1],
		"",
		rkn.DefaultSourceXLSXLSheet,
		rkn.DefaultSourceXLSXRowOffset)

	if err != nil {
		logger.Fatal(fmt.Sprintf("error processing xml: %s", err))
	}

	logger.Info("Done.")
}
