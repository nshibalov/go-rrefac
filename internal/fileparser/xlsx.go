package fileparser

import (
	"context"
	"fmt"

	"github.com/tealeg/xlsx"
)

type XLSXData struct {
	Row      *xlsx.Row
	RowID    int
	RowCount int
}

type XLSXParserCallback func(ctx context.Context, rowData *XLSXData) error

type XLSXParser interface {
	Parse(ctx context.Context, sheetName string, rowOffset, cellCount int, callback XLSXParserCallback) error
}

type xlsxParser struct {
	file *xlsx.File
}

func (p *xlsxParser) Parse(ctx context.Context, sheetName string, rowOffset, cellCount int, callback XLSXParserCallback) error {
	var sheet *xlsx.Sheet

	if sheetName != "" {
		sheet = p.file.Sheet[sheetName]
		if sheet == nil {
			return fmt.Errorf("no such sheet: %s", sheetName)
		}
	} else if len(p.file.Sheets) == 0 {
		return fmt.Errorf("no sheets")
	} else {
		sheet = p.file.Sheets[0]
	}

	for i, row := range sheet.Rows {
		if i < rowOffset || len(row.Cells) < cellCount {
			continue
		}

		err := callback(ctx, &XLSXData{
			Row:      row,
			RowID:    i + 1,
			RowCount: len(sheet.Rows),
		})

		if err != nil {
			return err
		}
	}

	return nil
}

func NewXLSXParser(fileName string) (XLSXParser, error) {
	file, err := xlsx.OpenFile(fileName)
	if err != nil {
		return nil, err
	}

	return &xlsxParser{
		file: file,
	}, nil
}
