package rkn_test

import (
	"context"
	"testing"

	"go.uber.org/zap"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"go-rrefac/internal/rkn"
)

type apiTestSuite struct {
	suite.Suite

	logger *zap.Logger
}

func (s *apiTestSuite) SetupTest() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		s.T().Fatal(err)
	}

	s.logger = logger

}

func (s *apiTestSuite) TearDownTest() {
	_ = s.logger.Sync()
}

func (s *apiTestSuite) TestGetREFacilityInfo() {
	config := rkn.NewConfig()

	api, err := rkn.NewAPI(s.logger)
	require.Nil(s.T(), err)

	resp, err := api.GetREFacilityInfo(context.Background(), &rkn.REFacility{
		INN:    config.INN,
		OGRN:   config.OGRN,
		Series: "77 18",
		Number: "19489",
	})

	require.Nil(s.T(), err)
	assert.NotNil(s.T(), resp)
}

func (s *apiTestSuite) TestGetREFacilityNotFound() {
	api, err := rkn.NewAPI(s.logger)
	require.Nil(s.T(), err)

	config := rkn.NewConfig()

	_, err = api.GetREFacilityInfo(context.Background(), &rkn.REFacility{
		INN:    config.INN,
		OGRN:   config.OGRN,
		Series: "77 18",
		Number: "1",
	})

	assert.ErrorIs(s.T(), err, rkn.ErrNotFound)
}

func TestAPITestSuite(t *testing.T) {
	suite.Run(t, new(apiTestSuite))
}
