package rkn

import "errors"

var (
	ErrGetREFacilityInfoFailed = errors.New("failed to check RE facility")
	ErrForbidden               = errors.New("forbidden")
	ErrNotFound                = errors.New("not found")
)
