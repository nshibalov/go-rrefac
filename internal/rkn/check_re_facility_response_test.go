package rkn_test

import (
	"testing"
	"time"

	"go.uber.org/zap"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"go-rrefac/internal/rkn"
)

type GetREFacilityInfoResponseTestSuite struct {
	suite.Suite

	logger *zap.Logger
}

func (s *GetREFacilityInfoResponseTestSuite) SetupTest() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		s.T().Fatal(err)
	}

	s.logger = logger

}

func (s *GetREFacilityInfoResponseTestSuite) TearDownTest() {
	_ = s.logger.Sync()
}

func (s *GetREFacilityInfoResponseTestSuite) TestResponseParser() {
	resp := rkn.GetREFacilityInfoResponse{
		HTML: `
		<div></div>
		<h1 style="font-family: 'Open Sans', sans-serif; font-weight:normal; font-size:20px;">Результат поиска по реестру</h1>
		<table width="100%" class="TblList">
			<tr>
				<td>Серия и номер реестровой записи</td>
				<td>77 18 19490</td>
			</tr>
			<tr>
				<td>Тип РЭС по ЕТС</td>
				<td>19.2. радиорелейная станция</td>
			</tr>
			<tr>
				<td>Статус записи</td>
				<td>действующая</td>
			</tr>
			<tr>
				<td>Дата начала действия</td>
				<td>09.04.2018</td>
			</tr>
			<tr>
				<td>Дата окончания действия</td>
				<td>01.06.2026</td>
			</tr>
			<tr>
				<td>Дата исключения</td>
				<td>-</td>
			</tr>
			<tr>
				<td class="tit" colspan="2"><b>Владелец РЭС</b></td>
			</tr>
			<tr>
				<td>Наименование / ФИО</td>
				<td>Публичное акционерное общество "Мобильные ТелеСистемы"</td>
			</tr>
			<tr>
				<td>ИНН</td>
				<td>7740000076</td>
			</tr>
			<tr>
				<td>ОГРН / ОГРНИП</td>
				<td>1027700149124</td>
			</tr>
		</table><br>
	`,
	}

	info, err := resp.ParseREFacilityInfo()
	require.Equal(s.T(), nil, err)

	activeFromDate := time.Date(2018, 4, 9, 0, 0, 0, 0, time.UTC)
	activeToDate := time.Date(2026, 6, 1, 0, 0, 0, 0, time.UTC)

	assert.Equal(s.T(), "77 18 19490", info.SeriesAndNumber)
	assert.Equal(s.T(), "19.2. радиорелейная станция", info.Type)
	assert.Equal(s.T(), "действующая", info.Status)
	assert.Equal(s.T(), &activeFromDate, info.ActiveFromDate)
	assert.Equal(s.T(), &activeToDate, info.ActiveToDate)
	assert.Nil(s.T(), info.DateOfExclusion)

}

func TestGetREFacilityInfoResponseTestSuite(t *testing.T) {
	suite.Run(t, new(GetREFacilityInfoResponseTestSuite))
}
