package rkn

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"
	"strings"
	"time"

	"go.uber.org/zap"

	"golang.org/x/net/publicsuffix"
)

const (
	UserAgent               = "rrefac"
	RequestTimeout          = 25 * time.Second
	EndpointHost            = "https://rkn.gov.ru/"
	EndpointCommRegisterRES = EndpointHost + "communication/register/registerRes/"
)

type api struct {
	logger *zap.Logger

	httpCli *http.Client
}

func (a *api) resetAuthorization() error {
	req, err := http.NewRequest("GET", EndpointCommRegisterRES, nil)
	if err != nil {
		return err
	}

	a.httpCli.Jar.SetCookies(req.URL, nil)

	return nil
}

func (a *api) getCode(cookies []*http.Cookie) int {
	var resP string

	for _, cookie := range cookies {
		if cookie.Name == "__js_p_" {
			resP = cookie.Value
			break
		}
	}

	parts := strings.Split(resP, ",")

	code, err := strconv.ParseInt(parts[0], 10, 32)
	if err != nil {
		return -1
	}

	return int(code)
}

func (a *api) getJHash(b int) int {
	x := 123456789
	i := 0
	k := 0

	for i = 0; i < 1677696; i++ {
		x = ((x + b) ^ (x + (x % 3) + (x % 17) + b) ^ i) % 16776960
		if x%117 == 0 {
			k = (k + 1) % 1111
		}
	}

	return k
}

func (a *api) Authorize(ctx context.Context) error {
	if err := a.resetAuthorization(); err != nil {
		return err
	}

	// part A

	req, err := http.NewRequestWithContext(ctx, "GET", EndpointHost, nil)
	if err != nil {
		return err
	}

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return err
	}

	resp.Body.Close()

	// part B

	// TODO: add checks for -1
	code := a.getCode(resp.Cookies())
	jHash := a.getJHash(code)

	a.httpCli.Jar.SetCookies(req.URL, append(a.httpCli.Jar.Cookies(req.URL),
		&http.Cookie{
			Name:  "__jhash_",
			Value: fmt.Sprint(jHash),
		},
		&http.Cookie{
			Name:  "__jua_",
			Value: UserAgent,
		},
	))

	time.Sleep(1000 * time.Millisecond)

	resp, err = a.httpCli.Do(req)
	if err != nil {
		return err
	}

	resp.Body.Close()

	return nil
}

func (a *api) GetREFacilityInfo(ctx context.Context, reFacility *REFacility) (*REFacilityInfo, error) {
	data := url.Values{}

	data.Set("inn", reFacility.INN)
	data.Set("ogrn", reFacility.OGRN)
	data.Set("seria", reFacility.Series)
	data.Set("num", reFacility.Number)

	req, err := http.NewRequestWithContext(ctx, "POST", EndpointCommRegisterRES, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusOK:
		var msg GetREFacilityInfoResponse

		err = json.NewDecoder(resp.Body).Decode(&msg)
		if err != nil {
			return nil, fmt.Errorf("%w: failed to parse RE response", err)
		}

		info, err := msg.ParseREFacilityInfo()

		switch {
		case errors.Is(err, ErrNotFound):
			return nil, err
		case err != nil:
			return nil, fmt.Errorf("%w: failed to parse RE info", err)
		}

		return info, nil

	case http.StatusForbidden:
		return nil, ErrForbidden
	}

	{ // log body
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		a.logger.Debug(fmt.Sprintf("GetREFacilityInfo: Response body:\n%s", string(body)))
	}

	return nil, fmt.Errorf("%w: unexpected code %d", ErrGetREFacilityInfoFailed, resp.StatusCode)
}

func NewAPI(logger *zap.Logger) (API, error) {
	ctx := context.Background()

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return nil, err
	}

	httpCli := &http.Client{
		Timeout: RequestTimeout,
		Jar:     jar,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			logger.Debug(fmt.Sprintf("redirect: %v, %v", req, via))

			return nil
		},
	}

	a := &api{
		logger:  logger,
		httpCli: httpCli,
	}

	err = a.Authorize(ctx)
	if err != nil {
		return nil, err
	}

	return a, nil
}
