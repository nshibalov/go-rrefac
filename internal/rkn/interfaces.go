package rkn

import "context"

type API interface {
	GetREFacilityInfo(ctx context.Context, reFacility *REFacility) (*REFacilityInfo, error)
}

type REFacilityReport interface {
	ProcessXLSX(ctx context.Context, sourceFileName, targetFileName, sheetName string, rowOffset int) (newTargetFileName string, err error)
}
