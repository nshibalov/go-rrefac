package rkn_test

import (
	"context"
	"testing"

	"go.uber.org/zap"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"go-rrefac/internal/rkn"
)

type reFacilityReportTestSuite struct {
	suite.Suite

	logger *zap.Logger
}

func (s *reFacilityReportTestSuite) SetupTest() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		s.T().Fatal(err)
	}

	s.logger = logger

}

func (s *reFacilityReportTestSuite) TearDownTest() {
	_ = s.logger.Sync()
}

func (s *reFacilityReportTestSuite) TestGetREFacilityInfo() {
	api, err := rkn.NewAPI(s.logger)
	require.Nil(s.T(), err)

	report := rkn.NewREFacilityReport(rkn.NewConfig(), api)

	_, err = report.ProcessXLSX(
		context.Background(),
		"../../test/example.xlsx",
		"",
		rkn.DefaultSourceXLSXLSheet,
		rkn.DefaultSourceXLSXRowOffset)

	require.Nil(s.T(), err)
}

func TestREFacilityReportTestSuite(t *testing.T) {
	suite.Run(t, new(reFacilityReportTestSuite))
}
