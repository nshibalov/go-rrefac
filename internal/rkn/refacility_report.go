package rkn

import (
	"context"
	"errors"
	"fmt"
	"go-rrefac/internal/fileparser"
	"path"
	"strings"
	"time"

	"github.com/tealeg/xlsx"
)

const (
	DefaultSourceXLSXLSheet    = "" // first
	DefaultSourceXLSXRowOffset = 1
	SourceXLSXCellCount        = 2
)

type reFacilityReport struct {
	config *Config

	api API
}

func (r *reFacilityReport) genTargetFileName(sourceFileName string) string {
	dir, base := path.Split(sourceFileName)
	ext := path.Ext(base)

	now := time.Now()

	return path.Join(
		dir,
		fmt.Sprintf("%s_%s.xlsx",
			strings.TrimSuffix(base, ext),
			now.Format("020106_030405")))
}

func (r *reFacilityReport) ProcessXLSX(ctx context.Context, sourceFileName, targetFileName, sheetName string, rowOffset int) (newTargetFileName string, err error) {
	xlsxParser, err := fileparser.NewXLSXParser(sourceFileName)
	if err != nil {
		return "", fmt.Errorf("failed to open source XLSX file: %w", err)
	}

	target := xlsx.NewFile()

	sheet, err := target.AddSheet("Report")
	if err != nil {
		return "", fmt.Errorf("failed to add sheet")
	}

	header := sheet.AddRow()
	if err != nil {
		return "", fmt.Errorf("failed to add header")
	}

	header.AddCell().Value = "Серия"
	header.AddCell().Value = "Номер"
	header.AddCell().Value = "Тип"
	header.AddCell().Value = "Статус"
	header.AddCell().Value = "Дата начала действия"
	header.AddCell().Value = "Дата окончания действия"
	header.AddCell().Value = "Дата исключения"

	err = xlsxParser.Parse(
		ctx,
		sheetName,
		rowOffset,
		SourceXLSXCellCount,
		func(ctx context.Context, rowData *fileparser.XLSXData) error {
			cells := rowData.Row.Cells

			series, number := cells[0].Value, cells[1].Value

			if series == "" || number == "" {
				return nil // skip
			}

			info, err := r.api.GetREFacilityInfo(ctx, &REFacility{
				INN:    r.config.INN,
				OGRN:   r.config.OGRN,
				Series: cells[0].Value,
				Number: cells[1].Value,
			})

			switch {
			case errors.Is(err, ErrNotFound):
				row := sheet.AddRow()
				if err != nil {
					return fmt.Errorf("failed to add row")
				}

				row.AddCell().Value = series
				row.AddCell().Value = number
				row.AddCell().Value = "-"
				row.AddCell().Value = "не найдена"
				row.AddCell().Value = "-"
				row.AddCell().Value = "-"
				row.AddCell().Value = "-"

			case err != nil:
				return err
			}

			row := sheet.AddRow()
			if err != nil {
				return fmt.Errorf("failed to add header")
			}

			row.AddCell().Value = series
			row.AddCell().Value = number
			row.AddCell().Value = info.Type
			row.AddCell().Value = info.Status

			if info.ActiveFromDate != nil {
				row.AddCell().Value = info.ActiveFromDate.Format(DateFormat)
			} else {
				row.AddCell().Value = "-"
			}

			if info.ActiveToDate != nil {
				row.AddCell().Value = info.ActiveToDate.Format(DateFormat)
			} else {
				row.AddCell().Value = "-"
			}

			if info.DateOfExclusion != nil {
				row.AddCell().Value = info.DateOfExclusion.Format(DateFormat)
			} else {
				row.AddCell().Value = "-"
			}

			return nil
		})

	if err != nil {
		return "", err
	}

	if targetFileName == "" {
		targetFileName = r.genTargetFileName(sourceFileName)
	}

	return targetFileName, target.Save(targetFileName)
}

func NewREFacilityReport(config *Config, api API) REFacilityReport {
	return &reFacilityReport{config: config, api: api}
}
