package rkn

type REFacility struct {
	INN    string `json:"inn"`
	OGRN   string `json:"ogrn"`
	Series string `json:"seria"`
	Number string `json:"num"`
}
