package rkn

import (
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	"golang.org/x/net/html"
)

const (
	DateFormat = "02.01.2006"
)

type GetREFacilityInfoResponse struct {
	Message string `json:"msg"`
	HTML    string `json:"html"`
}

type REFacilityInfo struct {
	SeriesAndNumber string
	Type            string
	Status          string

	ActiveFromDate  *time.Time
	ActiveToDate    *time.Time
	DateOfExclusion *time.Time
}

func (r *GetREFacilityInfoResponse) parseDate(value string) (*time.Time, error) {
	if value == "-" {
		return nil, nil
	}

	ret, err := time.Parse(DateFormat, value)
	if err != nil {
		return nil, fmt.Errorf("failed to parse date: %w", err)
	}

	return &ret, nil
}

func (r *GetREFacilityInfoResponse) ParseREFacilityInfo() (*REFacilityInfo, error) {
	if r.Message != "" {
		if r.Message == "Записей не найдено" {
			return nil, fmt.Errorf("%w: %s", ErrNotFound, r.Message)
		}

		return nil, errors.New(r.Message)
	}

	z := html.NewTokenizer(strings.NewReader(r.HTML))

	var (
		ret            REFacilityInfo
		textTokenValue string
		prevTDValue    string
	)

	for {
		if z.Next() == html.ErrorToken {
			if z.Err() == io.EOF {
				break
			}

			return nil, z.Err()
		}

		token := z.Token()

		switch {
		case token.Type == html.TextToken:
			textTokenValue = token.Data
			continue

		case token.Type == html.EndTagToken && token.Data == "td":
			// ok

		default:
			continue
		}

		switch prevTDValue {
		case "Серия и номер реестровой записи":
			ret.SeriesAndNumber = textTokenValue

		case "Тип РЭС по ЕТС":
			ret.Type = textTokenValue

		case "Статус записи":
			ret.Status = textTokenValue

		case "Дата начала действия":
			activeFromDate, err := r.parseDate(textTokenValue)
			if err != nil {
				return nil, err
			}

			ret.ActiveFromDate = activeFromDate

		case "Дата окончания действия":
			activeToDate, err := r.parseDate(textTokenValue)
			if err != nil {
				return nil, err
			}

			ret.ActiveToDate = activeToDate

		case "Дата исключения":
			dateOfExclusion, err := r.parseDate(textTokenValue)
			if err != nil {
				return nil, err
			}

			ret.DateOfExclusion = dateOfExclusion
		}

		prevTDValue = textTokenValue
	}

	return &ret, nil
}
