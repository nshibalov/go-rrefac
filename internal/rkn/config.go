package rkn

const (
	DefaultINN  = "7740000076"
	DefaultOGRN = "1027700149124"

	DefaultCheckEndpoint = "http://rkn.gov.ru/communication/register/registerRes/"
)

type Config struct {
	INN           string `yaml:"inn"`
	OGRN          string `yaml:"ogrn"`
	CheckEndpoint string `yaml:"checkEndpoint"`
}

func (c *Config) init() {
	if c.INN == "" {
		c.INN = DefaultINN
	}

	if c.OGRN == "" {
		c.OGRN = DefaultOGRN
	}

	if c.CheckEndpoint == "" {
		c.CheckEndpoint = DefaultCheckEndpoint
	}
}

func NewConfig() *Config {
	cfg := &Config{}
	cfg.init()

	return cfg
}
